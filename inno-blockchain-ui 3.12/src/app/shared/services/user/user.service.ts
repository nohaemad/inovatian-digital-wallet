import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { throwError, Observable } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { Router } from '@angular/router';
// import { UserInfo } from '../user.interfaces';
import 'rxjs/add/operator/catch';
//import 'rxjs/add/Observable/throw';
import { environment } from './../../../../environments/environment'
import * as sha256 from 'sha256';

@Injectable({
  providedIn: 'root'
})
export class UserService {


  api_url: string = environment.apiUrl;
  hash_password;
  constructor(private http: HttpClient, private router: Router) { }
  // Http Options
  httpOptions = {}

  /****************** start register  **********/
  addUser(user): Observable<HttpResponse<Object>> {
    // this.hash_password = sha256(user.password);

    var sendObject = { Name: user.name, Email: user.emailAdd, Phonnum: user.phone, Password: user.password, AuthenticationType: "passward", Address: user.address };

   
    console.log(JSON.stringify(sendObject));
    this.httpOptions = {
      headers: new HttpHeaders({
        // 'Content-Type': 'application/json,registerPage'//,'servicename':'registerPage'
                'Content-Type': 'application/json,registerPage'//,'servicename':'registerPage'

      })
    }
    return this.http.post<HttpResponse<object>>(this.api_url + '/UserRegister', JSON.stringify(sendObject), this.httpOptions)
      .catch(this.errorHandler);
  }

    /****************** end register  **********/


   /****************** start login  **********/

   userAuthentication(email, password): Observable<HttpResponse<Object>> {
    console.log(password);
    console.log('-- **** ' + sha256(password));
    this.hash_password = sha256(password);
    var sendObject = { EmailOrPhone: email, Password: this.hash_password,AuthValue: '' };
    console.log(JSON.stringify(sendObject));
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json,loginPage'//,'servicename':'loginPage'
      })
    }
    // const reqHeader = new HttpHeaders({ 'Content-Type': 'application/x-www-urlencoded', 'No-Auth': 'True' });
    return this.http.post<HttpResponse<object>>(this.api_url + '/Login', JSON.stringify(sendObject), this.httpOptions)

    .catch(this.errorHandler);

  }

  /****************** end login  **********/
  


  /***************** start  logout **********/

  logout() {
    localStorage.removeItem('PublicKey');
    this.router.navigate(['/auth/login']); 
    localStorage.clear();
  }

  /***************** end  logout **********/

 

  /****************** start error  **********/

  
  errorHandler(error: HttpErrorResponse) {
    return Observable.throw(error.message || "server Error");
  }

    /****************** end error  **********/

}
