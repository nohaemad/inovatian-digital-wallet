import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpHeaders, HttpErrorResponse } from "@angular/common/http";
import { Observable } from 'rxjs/Rx'

import { environment } from '../../../../environments/environment';
import { retry, catchError } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class AccountService {

  api_url: string = environment.apiUrl;
  // publicKey = new BehaviorSubject(null);
  httpOptions = {};
  name = JSON.parse(localStorage.getItem('userData')).AccountName;
  password = JSON.parse(localStorage.getItem('userData')).AccountPassword;
  public_key = JSON.parse(localStorage.getItem('userData')).AccountPublicKey;
  constructor(private http: HttpClient) { }

  // getPublicKeyByProperty(property) {
  //   return this.http
  //     .get(this.api_url + "/GetPublicKey", property)
  //     .subscribe(res => {
  //       this.publicKey.next(res);
  //     });
  // }

  getUnSpentTransactions() {
    return this.http.get(this.api_url + "/unSpentTransactions");
  }

  getReceive() {
    return this.http.get(this.api_url + '/');
  }

  search(text): Observable<HttpResponse<Object>> {
    // this.hash_password = sha256(user.password);

    var sendObject = { Name: this.name, Password: this.password, TextSearch: text.searchText };

    console.log(JSON.stringify(sendObject));
    this.httpOptions = {
      headers: new HttpHeaders({
        // 'Content-Type': 'application/json,registerPage'//,'servicename':'registerPage'
        'Content-Type': 'application/json,registerPage'//,'servicename':'registerPage'

      })
    }
    return this.http.post<HttpResponse<object>>(this.api_url + '/Search', JSON.stringify(sendObject), this.httpOptions)
      .catch(this.errorHandler);
  }

  /****************** start send transaction  **********/

  sendBalance(balanceDetails): Observable<HttpResponse<Object>> {
    var sendObject = { Receiver: balanceDetails.Reciever, Sender: balanceDetails.Sender, Amount: balanceDetails.Amount, Signature: balanceDetails.Hash };
    console.log(JSON.stringify(sendObject));
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'//,'servicename':'registerPage'
      })
    }
    return this.http.post<HttpResponse<object>>(this.api_url + '/AddNewTransaction', JSON.stringify(sendObject), this.httpOptions)

    .pipe(
      retry(1),
      catchError(this.errorHandler)
    );  }

  /****************** end send transaction  **********/

  /****************** start  get balance***********************/

  getBalance(PublicKey,Password){
    var sendObject = { PublicKey: PublicKey, Password: Password};
    if (sendObject.PublicKey != null){
    console.log(JSON.stringify(sendObject));
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'//,'servicename':'registerPage'
      })
    }
    return this.http.post<HttpResponse<object>>(this.api_url + '/GetBalance', JSON.stringify(sendObject), this.httpOptions)

    .pipe(
      retry(1),
      catchError(this.errorHandler)
    );   }
  }

  /****************** end  get balance***********************/

    /****************** start  get balance***********************/

    getTransactions(PublicKey,Password){
      var sendObject = { PublicKey: PublicKey, Password: Password};
      if (sendObject.PublicKey != null){
      console.log(JSON.stringify(sendObject));
      this.httpOptions = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json'//,'servicename':'registerPage'
        })
      }
      return this.http.post<HttpResponse<object>>(this.api_url + '/GetTransactionByPublicKey', JSON.stringify(sendObject), this.httpOptions)
  
      .pipe(
        retry(1),
        catchError(this.errorHandler)
      );  
       }   }
  
    /****************** end  get balance***********************/

  /****************** start error  **********/


  errorHandler(error: HttpErrorResponse) {
    return Observable.throw(error.message || "server Error");
  }

  /****************** end error  **********/

}
