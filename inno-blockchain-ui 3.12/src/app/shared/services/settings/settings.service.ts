import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { retry, catchError } from 'rxjs/operators';
import { throwError, Observable, from } from 'rxjs';
import { EditUserInfo, Deactivate } from '../../interfaces/user.interfaces';
import{environment} from'../../../../environments/environment'

@Injectable({
  providedIn: 'root'
})
export class SettingsService {


  api_url: string =environment.apiUrl;//'http://13.59.177.40:8090';
  name = JSON.parse(localStorage.getItem('userData')).AccountName;
  password = JSON.parse(localStorage.getItem('userData')).AccountPassword;

  public_key = JSON.parse(localStorage.getItem('userData')).AccountPublicKey;
  constructor(private http: HttpClient) { }

  // Http Options
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  }

  editUserInfo(user): Observable<EditUserInfo> {
    /* var sendObject = { 
       Name: user.name, 
       Password: this.password,
       Email : user.emailAdd , 
       Phonnum: user.phone , 
       Address : user.address , 
       PublicKey : this.public_key ,
       Status : "true" ,
       Role : "Admin" ,
       Reason : "change the status reason"
     };*/


    var sendObject = {
      Name: user.name,
      Password: this.password,
      Email: user.emailAdd,
      Phone: user.phone,
      Address: user.address,
      Publickey: this.public_key,
      OldPassword : this.password,
      Authentication: "passward",
      Role: '',
      AuthenticationValue: ''
    };
    // console.log(JSON.stringify("publickkkkkkkkkkk"));
    // console.log(sendObject.PublicKey);
    // console.log(JSON.stringify(sendObject));
    // console.log(sendObject.Phonnum);
    console.log(JSON.stringify(sendObject));

    return this.http.put<EditUserInfo>(this.api_url + '/UpdateAccountInfo', JSON.stringify(sendObject), this.httpOptions)
      .pipe(
        retry(1),
        catchError(
          this.handleError,
        )
      );
  }

  deactivateAccount(reason): Observable<Deactivate> {
    var sendObject = { 
      UserName: this.name, 
      PublicKey: this.public_key,
      Deactivation_reason: reason,
      Password: this.password,
    };
    return this.http.post<Deactivate>(this.api_url + "/ChangeStatus", JSON.stringify(sendObject), this.httpOptions)
      .pipe(
        retry(1),
        catchError(
          this.handleError,
        ));
  }

  handleError(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    // window.alert(errorMessage);
    return throwError(errorMessage);
  }
}
