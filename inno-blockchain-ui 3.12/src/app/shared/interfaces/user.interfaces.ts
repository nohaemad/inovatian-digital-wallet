export interface UserInfo{
    name: string;
    emailAdd: string;
    phone: string;
    address: string;
    authentication_type: string;
    authentication_value: string;
    password : string;
}

export interface EditUserInfo{
    name: string;
    email: string;
    phone: string;
    address: string;
}

export interface Deactivate{
    reason : string;

}