import { UserBalanceComponent } from "./user-balance/user-balance.component";
import { NgModule } from "@angular/core";

import { PagesComponent } from "./pages.component";
import { PagesRoutingModule } from "./pages-routing.module";
import { ThemeModule } from "../@theme/theme.module";
import { MiscellaneousModule } from "./miscellaneous/miscellaneous.module";
import { QrComponent } from "./qr/qr.component";
import { QRCodeModule } from "angularx-qrcode";
import { UserService } from "../@core/mock/users.service";
import { UploadService } from "../@core/mock/upload.service";

import { SearchResultComponent } from "./search-result/search-result.component";
import { SendComponent } from "./send/send.component";
import { ReceiveComponent } from "./receive/receive.component";
import { TransactionsComponent } from "./transactions/transactions.component";
import { EditProfileComponent } from './edit-profile/edit-profile.component';
import { InternationalPhoneModule } from 'ng4-intl-phone';
import { DeactivateComponent } from './deactivate/deactivate.component';
import { PrivacyPolicyComponent } from './privacy-policy/privacy-policy.component';
import { ProfileSidebarComponent } from './profile-sidebar/profile-sidebar.component';
import {NgxPaginationModule} from 'ngx-pagination';
import {NgxQRCodeModule} from 'ngx-qrcode2'
const PAGES_COMPONENTS = [PagesComponent];

@NgModule({
  imports: [
    PagesRoutingModule,
    ThemeModule,
    MiscellaneousModule,
    QRCodeModule,
    NgxPaginationModule,
    NgxQRCodeModule,
    InternationalPhoneModule
  ],
  declarations: [
    ...PAGES_COMPONENTS,
    QrComponent,
    SearchResultComponent,
    UserBalanceComponent,
    SendComponent,
    EditProfileComponent,
    ReceiveComponent,
    TransactionsComponent,
    DeactivateComponent,
    PrivacyPolicyComponent,
    ProfileSidebarComponent
    ],
  providers: [UserService, UploadService]
})
export class PagesModule {}
