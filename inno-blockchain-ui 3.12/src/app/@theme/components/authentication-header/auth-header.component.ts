import { Router } from "@angular/router";
import { Component, Input, OnInit } from "@angular/core";

import { NbMenuService, NbSidebarService } from "@nebular/theme";
import { UserData } from "../../../@core/data/users";
import { AnalyticsService } from "../../../@core/utils";
import { LayoutService } from "../../../@core/utils";
import {Location} from '@angular/common';

@Component({
  selector: "ngx-auth-header",
  styleUrls: ["./auth-header.component.scss"],
  templateUrl: "./auth-header.component.html"
})
export class AuthHeaderComponent implements OnInit {
  @Input() position = "normal";

  user: any;

  userMenu = [{ title: "Profile" }, { title: "Log out" }];
  userData:any;
  constructor(
    private sidebarService: NbSidebarService,
    private menuService: NbMenuService,
    private userService: UserData,
    private analyticsService: AnalyticsService,
    private layoutService: LayoutService,
    private router: Router,
    private location : Location
  ) {
    // this.userData = JSON.parse(localStorage.getItem('userData'));

  }

  ngOnInit() {
    this.userService
      .getUsers()
      .subscribe((users: any) => (this.user = users.nick));
  }

  toggleSidebar(): boolean {
    this.sidebarService.toggle(true, "menu-sidebar");
    this.layoutService.changeLayoutSize();

    return false;
  }

  goToHome() {
    this.menuService.navigateHome();
  }

  startSearch() {
    this.router.navigate(["/pages/search-result"]);
    this.analyticsService.trackEvent("startSearch");
  }

  onBack(){
    this.location.back();

  }
}
