import { Router } from "@angular/router";
import { Component, Input, OnInit } from "@angular/core";

import { NbMenuService, NbSidebarService } from "@nebular/theme";
import { UserData } from "../../../@core/data/users";
import { AnalyticsService } from "../../../@core/utils";
import { LayoutService } from "../../../@core/utils";
import { UserService } from '../../../shared/services/user/user.service';
import { ToastrService } from 'ngx-toastr';
import { Location } from '@angular/common';
import { AccountService } from '../../../shared/services/account/account.service';

@Component({
  selector: "ngx-header",
  styleUrls: ["./header.component.scss"],
  templateUrl: "./header.component.html"
})
export class HeaderComponent implements OnInit {
  @Input() position = "normal";

  user: any;
  username: string;
  image_url: string;
  userData = {};

  userDetails = {
    PublicKey: JSON.parse(localStorage.getItem('userData')).AccountPublicKey,
    Password: JSON.parse(localStorage.getItem('userData')).AccountPassword
  }

  balanceDetails :any ={
    TotalBalance : 0,
    TotalSent : 0,
    TotalReceived :0
  }
  userMenu = [{ title: "Profile" }, { title: "Log out" }];

  constructor(
    private sidebarService: NbSidebarService,
    private menuService: NbMenuService,
    private toastr: ToastrService,
    private userService: UserData,
    private analyticsService: AnalyticsService,
    private layoutService: LayoutService,
    private router: Router,
    private userSvr: UserService,
    private location: Location,
    private accountSvr: AccountService

  ) {
    this.userData = JSON.parse(localStorage.getItem('userData'));
    console.log(this.location.path());

  }

  ngOnInit() {
    // this.userService
    //   .getUsers()
    //   .subscribe((users: any) => (this.user = users.nick));
    this.username = JSON.parse(localStorage.getItem('userData')).AccountName;
    this.image_url = "../../assets/images/user.png"
    // if (this.userDetails.PublicKey != null){

    this.accountSvr.getBalance(this.userDetails.PublicKey, this.userDetails.Password).subscribe((data: any) => {
      localStorage.setItem('balanceDetails', JSON.stringify(data));
      console.log("balance details" + JSON.stringify(data));
      // if (!JSON.parse(localStorage.getItem('balanceDetails')).ErrorMessage) {
        

      this.balanceDetails = {
        TotalBalance : JSON.parse(localStorage.getItem('balanceDetails')).TotalBalance,
        TotalSent : JSON.parse(localStorage.getItem('balanceDetails')).TotalSent,
        TotalReceived : JSON.parse(localStorage.getItem('balanceDetails')).TotalReceived,


    }
      console.log( this.balanceDetails)
    // }else{
      // this.toastr.error(JSON.parse(localStorage.getItem('balanceDetails')).ErrorMessage)
    // }
    })
  // }
  

  

  
  }

  toggleSidebar(): boolean {
    this.sidebarService.toggle(true, "menu-sidebar");
    this.layoutService.changeLayoutSize();

    return false;
  }

  goToHome() {
    this.menuService.navigateHome();
  }

  startSearch() {
    // this.router.navigate(["/pages/search-result"]);
    this.analyticsService.trackEvent("startSearch");
  }
  logout() {
    this.userSvr.logout();
    this.toastr.success('logout is successful', 'Please login to go to your profile');

  }

  goToProfile() {
    this.router.navigate(["/pages/edit-profile"]);
  }

  goToDeactivate() {
    this.router.navigate(["/pages/deactivate-account"]);
  }
}
