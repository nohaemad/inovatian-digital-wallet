import { Injectable } from '@angular/core';
import { NotifierService } from 'angular-notifier';
@Injectable({
  providedIn: 'root'
})
export class ErrorhanleService {
  private readonly notifier: NotifierService;
  constructor(private notify:NotifierService) { 

   // this.notifier = notify;

  }
returnException(err){
  this.notify.show( {
    type: 'success',
    message: 'You are awesome! I mean it!',
    id: 'THAT_NOTIFICATION_ID' // Again, this is optional
  } );
  this.notifier.notify( 'success', 'You are awesome! I mean it!', 'THAT_NOTIFICATION_ID' );
}
public showNotification( type: string, message: string ): void {
  this.notify.notify( type, message );
}

/**
 * Hide oldest notification
 */
public hideOldestNotification(): void {
  this.notify.hideOldest();
}

/**
 * Hide newest notification
 */
public hideNewestNotification(): void {
  this.notify.hideNewest();
}

/**
 * Hide all notifications at once
 */
public hideAllNotifications(): void {
  this.notify.hideAll();
}


public showSpecificNotification( type: string, message: string, id: string ): void {
  this.notify.show( {
    id,
    message,
    type
  } );
}


public hideSpecificNotification( id: string ): void {
  this.notify.hide( id );
}


}
