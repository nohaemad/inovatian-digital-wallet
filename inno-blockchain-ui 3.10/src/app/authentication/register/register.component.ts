import { Component, OnInit, Input } from '@angular/core';
import { NgForm, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NotifierService } from 'angular-notifier';
import { ToastrService } from 'ngx-toastr';
import * as sha256 from 'sha256';
import { UserService } from '../../shared/services/user/user.service';
import * as $ from 'jquery';

@Component({
  selector: 'ngx-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss', '../pages.component.scss']
})
export class RegisterComponent implements OnInit {


  @Input() userDetails = { name: '', emailAdd: '', phone: '', address: '', password: '',confirmNew_password:'' }
  userobject: any = { name: '', emailAdd: '', phone: '', address: '', password: '' };
  hash_password;
  public errorMsg;

  constructor(private userSvr: UserService,
    private router: Router,
    private notify: NotifierService,
    private toastr: ToastrService) {
    // this.showNotification("success","welcome");
  }

  ngOnInit() {

    $(document).on('click', '.toggle-password', function () {

      $(this).toggleClass("eva-eye eva-eye-off");

      var input = $("#pass_log_id");
      input.attr('type') === 'password' ? input.attr('type', 'text') : input.attr('type', 'password')
    });

    $(document).on('click', '.toggle-password2', function () {

      $(this).toggleClass("eva-eye eva-eye-off");

      var input = $("#pass_log_id2");
      input.attr('type') === 'password' ? input.attr('type', 'text') : input.attr('type', 'password')
    });
  }
  // disabled:boolean;
  OnSubmit(form: NgForm) {
    console.log(form.value);
    // if(form.value.name==""||form.value.emailAdd==""|| form.value.password==""||(form.value.password !=form.value.confirm))
    //  this.disabled=false;
    if ((form.value.name === '' || form.value.name === null) ||
      (form.value.emailAdd === '' || form.value.emailAdd === null) ||
      (form.value.phone === '' || form.value.phone === null) ||
      (form.value.password === '' || form.value.password === null) ||
      (form.value.confirm === '' || form.value.confirm === null)) {
      if (form.value.name === '' || form.value.name === null) {
        this.toastr.error('Name is required');
      }
      if (form.value.emailAdd === '' || form.value.emailAdd === null) {
        if (form.value.phone === '' || form.value.phone === null) {
          this.toastr.error('phone is required');
        }
      }
      if (form.value.password === '' || form.value.password === null) {
        this.toastr.error('password is required');
      }
      if (form.value.confirmNew_password === '' || form.value.confirmNew_password === null) {
        this.toastr.error('confirm password is required');
      }
    }

    else {

      this.hash_password = sha256(form.value.password);
      this.userobject.name = form.value.name;
      this.userobject.emailAdd = form.value.emailAdd;
      this.userobject.phone = form.value.phone;
      this.userobject.address = form.value.address;
      this.userobject.password = this.hash_password;
      console.log(form.value.password);
      console.log(sha256(form.value.password));
      
      this.userSvr.addUser(this.userobject)
        .subscribe((data: any) => {
          localStorage.setItem('userResponses', JSON.stringify(data));
          if(!JSON.parse(localStorage.getItem('userResponses')).Message){

          this.toastr.success('Register is successful', 'Please check your email to confirm your account');
          this.router.navigate(['./auth/login']);

          // console.log(data)
          //notify("welcome");
          console.log(data);
          }
          else{
            this.toastr.error(JSON.parse(localStorage.getItem('userResponses')).Message)

          }
        })
    }
  }
}
