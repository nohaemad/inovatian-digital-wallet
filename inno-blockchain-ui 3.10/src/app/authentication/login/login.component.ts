import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../../shared/services/user/user.service';
import { ToastrService } from 'ngx-toastr';
import * as $ from 'jquery';

@Component({
  selector: 'ngx-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss', '../pages.component.scss']
})
export class LoginComponent implements OnInit {

  isLoggin: boolean = false;
  public errorMsg;
  @Input() userDetails = { emailAdd: '', password: '' };
  userobject: any = { emailAdd: '', password: '' };
  constructor(private userSvr: UserService, private router: Router, private toastr: ToastrService) { }

  ngOnInit() {
    $(document).on('click', '.toggle-password', function () {

      $(this).toggleClass("eva-eye eva-eye-off");

      var input = $("#pass_log_id");
      input.attr('type') === 'password' ? input.attr('type', 'text') : input.attr('type', 'password')
    });
  }

  onSubmit(emailAdd, password) {
    if ((emailAdd == "" || emailAdd == null) || (password == "" || password == null)) {
      if (emailAdd == "" || emailAdd == null) {
        this.toastr.error('Email is Required');
      }

      if (password == "" || password == null) {
        this.toastr.error('password is Required');
      }



    }

    else if ((emailAdd == "" || emailAdd == null) && (password == "" || password == null)) {
      this.toastr.error('you must enter email and password');
    }
    else {
      this.userobject.emailAdd = emailAdd;
      this.userobject.password = password;
      console.log("goooooooooooooooooooo");
      this.userSvr.userAuthentication(emailAdd, password)
        .subscribe((data: any) => {
          localStorage.setItem('userData', JSON.stringify(data));
          if (!JSON.parse(localStorage.getItem('userData')).Message) {
            this.router.navigate(['./pages/edit-profile']);
            this.toastr.success('Login Successfuly', '');
            console.log("logged in");

          } else {

            this.toastr.error(JSON.parse(localStorage.getItem('userData')).Message)
          }
        })


    }

  }
}
