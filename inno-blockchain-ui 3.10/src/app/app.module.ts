import { UserService } from "./@core/mock/user.service";
/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { APP_BASE_HREF } from "@angular/common";
import { BrowserModule } from "@angular/platform-browser";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { NgModule } from "@angular/core";
import { CoreModule } from "./@core/core.module";

import { AppComponent } from "./app.component";
import { AppRoutingModule } from "./app-routing.module";
import { ThemeModule } from "./@theme/theme.module";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { NbAuthModule } from '@nebular/auth';
import { PagesModule } from './authentication/pages.module';
import { ToastrModule } from 'ngx-toastr';
import { NotifierModule } from 'angular-notifier';
import { HttpClientModule, HTTP_INTERCEPTORS, HttpClient } from '@angular/common/http';
import{InoInterceptor} from './shared/services/ino.interceptor'
import { FormsModule } from '@angular/forms';
import { PaginationModule } from 'ngx-bootstrap/pagination';
@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,    FormsModule,
    ToastrModule.forRoot(
      {
        maxOpened: 1,
        progressBar: true,
      }
    ),
    NotifierModule,
    BrowserAnimationsModule,
    HttpClientModule,
    AppRoutingModule,
    NbAuthModule,
    PagesModule,
    NgbModule.forRoot(),
    ThemeModule.forRoot(),
    CoreModule.forRoot(),
    PaginationModule.forRoot()
  ],
  bootstrap: [AppComponent],
  providers: [UserService, { provide: APP_BASE_HREF, useValue: "/" },
  { provide: HTTP_INTERCEPTORS, useClass: InoInterceptor, multi: true },

]
})
export class AppModule {}
