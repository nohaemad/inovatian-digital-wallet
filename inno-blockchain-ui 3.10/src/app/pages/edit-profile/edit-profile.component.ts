import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { SettingsService } from '../../shared/services/settings/settings.service';
import { NgForm } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { UserData } from '../../@core/data/users';


@Component({
  selector: 'ngx-edit-profile',
  templateUrl: './edit-profile.component.html',
  styleUrls: ['./edit-profile.component.scss', '../pages.component.scss']
})
export class EditProfileComponent implements OnInit {


  userData;

  constructor(private settingsSvr: SettingsService, private router: Router, public actRoute: ActivatedRoute, private toastr: ToastrService) {
    // this.user_Data = JSON.parse(localStorage.getItem('userData')).publicKey;
    // this.publicKey=this.user_Data.PublicKey;
  }

  ngOnInit() {

    this.userData = {
      name: JSON.parse(localStorage.getItem('userData')).AccountName,
      emailAdd: JSON.parse(localStorage.getItem('userData')).AccountEmail,
      phone: JSON.parse(localStorage.getItem('userData')).AccountPhoneNumber,
      address: JSON.parse(localStorage.getItem('userData')).AccountAddress,
    }
  }

  onSubmit(form: NgForm) {
    if (window.confirm('Are you sure, you want to update?')) {
      if ((form.value.name === '' || form.value.name === null) ||
        (form.value.emailAdd === '' || form.value.emailAdd === null) ||
        (form.value.phone === '' || form.value.phone === null) ||
        (form.value.address === '' || form.value.address === null)) {

        if (form.value.name === '' || form.value.name === null) {
          this.toastr.error('Name is required');
        }

        if (form.value.emailAdd === '' || form.value.emailAdd === null) {
          this.toastr.error('Email is required');
        }

        // if (form.value.emailAdd === '' || form.value.emailAdd === null) {
        //   if (form.value.phone === '' || form.value.phone === null) {
        //     this.toastr.error('phone is required');
        //   }
        // }

        if (form.value.phone === '' || form.value.phone === null) {
          this.toastr.error('phone is required');
        }
        if (form.value.address === '' || form.value.address === null) {
          this.toastr.error('address is required');
        }

      } else {
        this.settingsSvr.editUserInfo(form.value).subscribe((data: any) => {

          localStorage.setItem("editResponse",JSON.stringify(data));

          if (!JSON.parse(localStorage.getItem('editResponse')).Message) {

            this.toastr.success('edit is successful', 'Please check your email to confirm your account');

            // localStorage.setItem("Status", "true");
            // localStorage.setItem("Role", "Admin");
            //  localStorage.setItem("Reason", "change the status reason");
            // this.router.navigate(['/pages/edit-profile']);
            localStorage.setItem("userData", JSON.stringify(data));

          }

          else {
            this.toastr.error(JSON.parse(localStorage.getItem('editResponse')).Message)

          }
        })
      }
    }
  }

  cancel() {
    if (window.confirm('Are you sure, you want to cancel your update?')) {
      this.toastr.error('you cancel edits');
      this.router.navigate(['/pages/transactions']);

    }
  }
}
