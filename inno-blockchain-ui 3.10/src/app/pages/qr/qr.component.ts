import { UserService } from "./../../@core/mock/user.service";
import { Component, OnInit } from "@angular/core";

@Component({
  selector: "ngx-qr",
  templateUrl: "./qr.component.html",
  styleUrls: ["./qr.component.scss", '../pages.component.scss']
})
export class QrComponent implements OnInit {
  myAngularxQrCode: string;
  public_key = JSON.parse(localStorage.getItem('userData')).AccountPublicKey;
  constructor(private userService: UserService) {
    this.myAngularxQrCode = this.public_key;
  }

  ngOnInit() {}
}
