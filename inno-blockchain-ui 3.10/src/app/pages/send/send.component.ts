import { Component, OnInit, Input } from "@angular/core";
import { Router } from "@angular/router";
import { AnalyticsService } from "../../@core/utils";
import { NgForm } from '@angular/forms';
import { UserService } from '../../shared/services/user/user.service';
import { ToastrService } from 'ngx-toastr';
import { AccountService } from '../../shared/services/account/account.service';
declare const setParam: any//(from:string,to:string,amount:number,pwd:string);
declare const gethSig: any//();
import * as $ from 'jquery';

@Component({
  selector: "ngx-send",
  templateUrl: "./send.component.html",
  styleUrls: ["./send.component.scss", '../pages.component.scss']
})
export class SendComponent implements OnInit {
  uploadResponse = { status: "", message: "", filePath: "" };
  userData: any;
  publicKey: string;
  @Input() sendDetails = { Reciever: '', Sender: '', Amount: 0, Hash: '' };
  @Input() searchDetails = { searchText: '' };
  public errorMsg;
  stringAmount: string;
  receiverDetails: any = [];

  balanceDetails= {
    TotalBalance: JSON.parse(localStorage.getItem('balanceDetails')).TotalBalance,
  }

  senderDetails = {
    PublicKey: JSON.parse(localStorage.getItem('userData')).AccountPublicKey,
    PrivateKey: JSON.parse(localStorage.getItem('userData')).AccountPrivateKey,
    Status: JSON.parse(localStorage.getItem('userData')).AccountStatus
  }

  constructor(
    private analyticsService: AnalyticsService,
    private router: Router,
    private userSvr: UserService,
    private toastr: ToastrService,
    private accountSvr: AccountService
  ) {

  }
  disabled: boolean;
  ngOnInit() {
    if (JSON.parse(localStorage.getItem("searchDetails"))) {
      this.receiverDetails = {
        PublicKey: JSON.parse(localStorage.getItem('searchDetails')).PublicKey,
        UserName: JSON.parse(localStorage.getItem('searchDetails')).UserName
      }

    }
  }
  public imagePath;
  imgURL: any;
  public message: string;
  OnSubmit(form: NgForm) {
    // if(form.value.Reciever==null || form.value.Reciever=="" || parseInt(form.value.Amount)<=0)
    // this.disabled=true;
    this.userData = JSON.parse(localStorage.getItem('userData'));
    console.log(this.userData)

    if (form.value.Amount == "" || form.value.Amount == null || (parseInt(form.value.Amount) == 0)) {
      this.toastr.error('Amount is required');
    }

    else if(form.value.Amount > this.balanceDetails.TotalBalance){
      this.toastr.error('your balance is not enough');

    }

    //  if ((parseInt(form.value.Amount)==null ||parseInt(form.value.Amount) <= 0) && (parseInt(this.userData.Balance) > parseInt(form.value.Amount))) {
    //     this.toastr.warning('your ammount must be greater than 0');
    //   }
    else if (parseInt(form.value.Amount) > 0 && (parseInt(this.userData.Balance) < parseInt(form.value.Amount))) {
      this.toastr.warning('sorry,your balanc is not enough');
    }
    else if (parseInt(form.value.Amount) < 0) {
      this.toastr.warning('sorry,your balanc must be greater than 0');
    }

    else {
      this.publicKey = this.userData.PublicKey;
      console.log(form.value);
      // this.sendDetails.Reciever = this.receiverDetails.PublicKey
      this.sendDetails.Sender = this.senderDetails.PublicKey
      this.sendDetails.Amount = parseInt(form.value.Amount)
      // this.stringAmount = this.sendDetails.Amount.toString();
      this.sendDetails.Reciever = this.receiverDetails.PublicKey
      // rsa encryption 
      var privKey = this.senderDetails.PrivateKey;
      console.log("private key : " + privKey);
      setParam(this.sendDetails.Sender, this.sendDetails.Reciever, this.sendDetails.Amount, privKey);
      var h_sig = gethSig()
      this.sendDetails.Hash = h_sig
      console.log("signature : " + h_sig)
      // this.sendDetails.password=form.value.password
      this.accountSvr.sendBalance(this.sendDetails)
        .subscribe((data: any) => {
          localStorage.setItem('Responses', JSON.stringify(data));
          if (!JSON.parse(localStorage.getItem('Responses')).ErrorMessage) {
            this.toastr.success('Your transaction with '+this.sendDetails.Amount+ ' coin has been sent successfully to '+this.receiverDetails.UserName);
            this.router.navigate(['./pages/transactions']);
            this.userData.Balance = parseInt(this.userData.Balance) - parseInt(form.value.Amount)
            localStorage.removeItem("userData");
            localStorage.setItem("userData", JSON.stringify(this.userData))
          }
          else {
            this.toastr.error(JSON.parse(localStorage.getItem('Responses')).ErrorMessage)
          }
          // console.log(data)
          //notify("welcome");
        })
    
    }
  }

  onSearch(form: NgForm) {
    console.log(form.value);
    this.searchDetails.searchText = form.value.searchText;
    this.accountSvr.search(this.searchDetails).subscribe((data: any) => {
      localStorage.setItem('searchDetails', JSON.stringify(data));
      if (!JSON.parse(localStorage.getItem('searchDetails')).ErrorMessage) {

        $('.modal').removeClass('show');
        $('.modal-backdrop.show').hide();
        $('body').css("padding-right", "0");
        this.toastr.success('Search is Successfully');
        location.reload();
      }
      else {
        this.toastr.error(JSON.parse(localStorage.getItem('searchDetails')).ErrorMessage)
      }
    });
  }

  // preview(files) {
  //   if (files.length === 0) return;

  //   var mimeType = files[0].type;
  //   if (mimeType.match(/image\/*/) == null) {
  //     this.message = "Only images are supported.";
  //     return;
  //   }

  //   var reader = new FileReader();
  //   this.imagePath = files;
  //   reader.readAsDataURL(files[0]);
  //   reader.onload = _event => {
  //     this.imgURL = reader.result;
  //   };
  // }
  // startSearch() {
  //   // this.router.navigate(["/pages/search-result"]);
  //   this.analyticsService.trackEvent("startSearch");
  // }
}
