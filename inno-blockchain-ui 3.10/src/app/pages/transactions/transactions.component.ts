import { Component, OnInit,AfterContentInit } from '@angular/core';
import { AccountService } from '../../shared/services/account/account.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'ngx-transactions',
  templateUrl: './transactions.component.html',
  styleUrls: ['./transactions.component.scss', '../pages.component.scss']
})
export class TransactionsComponent implements OnInit{

  userDetails = {
    PublicKey: JSON.parse(localStorage.getItem('userData')).AccountPublicKey,
    Password: JSON.parse(localStorage.getItem('userData')).AccountPassword
  }

  transactions: any = [];
  // maxSize = 5;
  // bigTotalItems = 175;
  // bigCurrentPage = 1;
  constructor(private accountSvr: AccountService,private toastr : ToastrService) {
    
   }

  ngOnInit() {
    if (this.userDetails.PublicKey != null){

    this.accountSvr.getTransactions(this.userDetails.PublicKey, this.userDetails.Password).subscribe((transactions: any) => {
      localStorage.setItem('transactionDetails', JSON.stringify(transactions));
      if (!JSON.parse(localStorage.getItem('transactionDetails')).ErrorMessage) {

      this.transactions = JSON.parse(localStorage.getItem("transactionDetails")).Transactions;
      }else{
        this.toastr.error(JSON.parse(localStorage.getItem('transactionDetails')).ErrorMessage)
      }
    })

  }

// ngAfterContentInit(){
//   if (this.userDetails.PublicKey != null){

//     this.accountSvr.getTransactions(this.userDetails.PublicKey, this.userDetails.Password).subscribe((transactions: any) => {
//       localStorage.setItem('transactionDetails', JSON.stringify(transactions));
//       if (!JSON.parse(localStorage.getItem('transactionDetails')).ErrorMessage) {

//       this.transactions = JSON.parse(localStorage.getItem("transactionDetails")).Transactions;
//       }else{
//         this.toastr.error(JSON.parse(localStorage.getItem('transactionDetails')).ErrorMessage)
//       }
//     })

//   }

// }

  
  }
  }

