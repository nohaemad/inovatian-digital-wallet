import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { environment } from "../../../environments/environment";
import { BehaviorSubject } from "rxjs";

@Injectable()
export class UserService {
  publicKey = new BehaviorSubject(null);
  constructor(private http: HttpClient) {}

  getPublicKeyByProperty(property) {
    return this.http
      .get(environment.apiUrl + "/GetPublicKey", property)
      .subscribe(res => {
        this.publicKey.next(res);
      });
  }

  getUnSpentTransactions() {
    return this.http.get(environment.apiUrl + "/unSpentTransactions");
  }
}
